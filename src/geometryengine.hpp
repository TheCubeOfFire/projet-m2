#ifndef GEOMETRYENGINE_HPP
#define GEOMETRYENGINE_HPP

#include "btlx.hpp"

#include <QImage>
#include <QMatrix4x4>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLWidget>
#include <QQuaternion>
#include <QVector3D>

#include <iterator>
#include <stdexcept>
#include <utility>
#include <vector>

#include <cstddef>

class Geometry: protected QOpenGLFunctions {
public:
    explicit Geometry(QOpenGLWidget& widget, const QString& shaderName, bool ebo = true);
    virtual ~Geometry() noexcept;

    Geometry(const Geometry&) = delete;
    Geometry(Geometry&&) = delete;

    Geometry& operator=(const Geometry&) = delete;
    Geometry& operator=(Geometry&&) = delete;

protected:
    template<typename... T>
    void shaderUniform(T&&... args)
    {
        m_program.setUniformValue(std::forward<T>(args)...);
    }

    void shaderAttribute(const char* name, std::size_t offset, GLenum type, std::size_t count, std::size_t stride);
    void shaderAttribute(const char* name, GLenum type, std::size_t count);

    template<typename T>
    void vboAllocate(const T& container)
    {
        m_widget->makeCurrent();
        m_arrayBuf.bind();
        m_arrayBuf.allocate(container.data(), static_cast<int>(container.size() * sizeof(typename T::value_type)));
        m_widget->doneCurrent();
    }

    template<typename T>
    void eboAllocate(const T& container)
    {
        Q_ASSERT(m_ebo);
        m_widget->makeCurrent();
        m_indexBuf.bind();
        m_indexBuf.allocate(container.data(), static_cast<int>(container.size() * sizeof(typename T::value_type)));
        m_widget->doneCurrent();
    }

    void beginDraw()
    {
        m_program.bind();
        m_arrayBuf.bind();
        if (m_ebo) {
            m_indexBuf.bind();
        }
    }

    void endDraw()
    {
        m_program.release();
        m_arrayBuf.release();
        if (m_ebo) {
            m_indexBuf.release();
        }
    }

protected:
    QOpenGLWidget& widget()
    {
        return *m_widget;
    }

private:
    QOpenGLWidget* m_widget;
    bool m_ebo;

    QOpenGLShaderProgram m_program;

    QOpenGLBuffer m_arrayBuf;
    QOpenGLBuffer m_indexBuf;
};

class OverlayGeometry: public Geometry {
public:
    explicit OverlayGeometry(QOpenGLWidget& widget);
    virtual ~OverlayGeometry() noexcept;

    void draw();
    void setOverlay(QImage&& overlay);

private:
    static constexpr unsigned vertexCount = 4;

    QOpenGLTexture* m_texture;
};

class AxesGeometry: public Geometry {
public:
    explicit AxesGeometry(QOpenGLWidget& widget);

    void draw(const QQuaternion& rotation, float aspect);

private:
    static constexpr unsigned vertexCount = 6;
};

class ShapesGeometry: public Geometry {
public:
    explicit ShapesGeometry(QOpenGLWidget& widget):
        Geometry{widget, "wireframe"},
        m_project{nullptr}
    {
    }

    void setProject(const Btlx::Project& project);

    void updateGeometry();
    void updateGlobalTransform(std::size_t index, std::size_t transformIndex);
    void updateRawPartTransform(std::size_t index, std::size_t transformIndex);

    void drawProject(const QMatrix4x4& view, const QMatrix4x4& projection);
    void drawRawPart(std::size_t index, const QMatrix4x4& view, const QMatrix4x4& projection, std::ptrdiff_t transformIndex = -1);
    void drawPart(std::size_t index, const QMatrix4x4& view, const QMatrix4x4& projection);

private:
    void clear();
    std::vector<std::size_t> getShape(const QList<QList<int>>& indices, const QList<QVector3D>& points);
    void addFace(const QList<int>& indices, std::vector<std::size_t>& shapeFaces);
    void drawShape(const std::vector<std::size_t>& shape, bool highlight = false);

private:
    struct PartElement {
        std::vector<std::size_t> faces;
        std::vector<QMatrix4x4> tranforms;
    };

    struct LinkElement {
        std::size_t index;
        QMatrix4x4 transform;
    };

    struct RawPartElement {
        std::vector<std::size_t> faces;
        std::vector<LinkElement> links;
    };

    const Btlx::Project* m_project;

    std::vector<QVector3D> m_points;
    std::vector<unsigned> m_indices;
    std::vector<PartElement> m_parts;
    std::vector<RawPartElement> m_rawParts;
};

struct GeometryEngine {
    explicit GeometryEngine(QOpenGLWidget& widget):
        axes{widget},
        shapes{widget},
        overlay{widget}
    {
    }

    static constexpr float unitLength = 1E-3F;

    AxesGeometry axes;
    ShapesGeometry shapes;
    OverlayGeometry overlay;
};

#endif // GEOMETRYENGINE_HPP
