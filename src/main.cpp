#include "mainwindow.hpp"

#include <QApplication>

#include <exception>
#include <iostream>

int main(int argc, char *argv[]) try
{
    Q_INIT_RESOURCE(resources);
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
} catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
}
