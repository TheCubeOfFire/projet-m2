#ifndef PART_HPP
#define PART_HPP

#include <QDomDocument>
#include <QList>
#include <QMatrix4x4>
#include <QVector3D>
#include <QUrl>

#include <vector>

#include <cstddef>

namespace Btlx {

enum class ItemType {
    Project,
    RawPart,
    PartLink,
    Part
};
struct RefSide
{
    QString align;
    int side;
};
struct Color{
    int blue;
    int red;
    int green;
    int transparency;
};

struct Cut {
    QString contenu;
};

struct Position {
    QString type;
    QString guid;
    QVector3D refPoint;
    QVector3D xvector;
    QVector3D yvector;

    QMatrix4x4 getTransform() const
    {
        QMatrix4x4 transform;
        transform.translate(refPoint);

        // compute unit vectors
        auto ux = xvector.normalized();
        auto tmpuy = yvector.normalized();
        auto uz = QVector3D::normal(ux, tmpuy);
        auto uy = QVector3D::normal(uz, ux); // ensures all vectors are orthogonal

        if (!ux.isNull() && !uy.isNull() && !uz.isNull()) {
            QMatrix4x4 rotation;
            rotation.setColumn(0, ux);
            rotation.setColumn(1, uy);
            rotation.setColumn(2, uz);

            transform = transform * rotation;
        }

        return transform;
    }
};

struct PartLink {
    std::size_t partIndex;
    std::size_t rpartTransformIndex;
};

struct RawPart {
    QVector3D size;
    QString designation;
    QString material;
    int elementnumber;
    int count;
    QList<QList<int>> indexCoor;
    QList<QVector3D> points;
    int number;
    QList<Position> partref;
    QList<Position> userrefplane;
    QList<Position> transformation;
    RefSide rf;
    std::vector<PartLink> links;
};

struct Part {
    QVector3D size;
    QString designation;
    QString material;
    QString storey;
    QString group;
    int ordernumber;
    int assemblynumber;
    int elementnumber;
    int count;
    QList<QList<int>> indexCoor;
    QList<QVector3D> points;
    int number;
    QList<Position> userrefplane;
    QList<Position> transformation;
    QList<Cut> cuts;
    RefSide rf;
    Color color;
};

struct Project {
    QUrl emplacement;
    QDomDocument domDoc;
    std::vector<Part> parts;
    std::vector<RawPart> rparts;
};

} // namespace Btlx

#endif // PART_HPP
