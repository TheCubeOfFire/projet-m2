#ifndef LOADSAVEXML_H
#define LOADSAVEXML_H


#include "btlx.hpp"

#include <QFile>
#include <QUrl>
#include <QXmlQuery>
#include <QtXml>

#include <vector>

namespace load {
void loadcolorpart(QXmlQuery &query,std::vector<Btlx::Part> &parts);
void loadrefsiderawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts);
void loadrefsidepart(QXmlQuery &query,std::vector<Btlx::Part> &parts);
void loadtransformationrawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts);
void loadpartrefrawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts);
void loadrefplanerawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts);
void loadrefplanerawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts);
void loadtransformationpart(QXmlQuery &query,std::vector<Btlx::Part> &parts);
void loadrefplanepart(QXmlQuery &query,std::vector<Btlx::Part> &parts);
void loadrawpartshape(QXmlQuery& query, std::vector<Btlx::RawPart>& rparts);
void loadrawparts(QXmlQuery& query, std::vector<Btlx::RawPart>& rparts);
void loadpartshape(QXmlQuery& query, std::vector<Btlx::Part>& parties);
void loadparts(QXmlQuery& query, std::vector<Btlx::Part>& parts);
Btlx::Project loadfile(const QUrl& url);
} // namespace load

namespace save {
void saveProject(Btlx::Project& project, QFile& file);
} // namespace save

#endif // LOADSAVEXML_H
