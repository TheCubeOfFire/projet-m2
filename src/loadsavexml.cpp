#include "loadsavexml.hpp"

#include <QByteArray>
#include <QFile>
#include <QIODevice>
#include <QString>

#include <unordered_map>
#include <utility>

namespace load {
void loadtransformationrawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Rawparts/Rawpart/Transformations
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i) {
        QList<Btlx::Position> transfo;
        QDomNodeList transfosChildNodes = partNodes.item(i).toElement().childNodes();
        if (partNodes.item(i).isElement()) {
            for(int j =0;j<transfosChildNodes.length();j++){
            QDomElement part = transfosChildNodes.item(j).toElement();
            Btlx::Position p;
            p.type = "Transformation";
            QString guid = part.attribute("GUID", "");
            QDomNodeList transfoo = transfosChildNodes.item(j).toElement().childNodes();
            QDomNodeList pos = transfoo.item(0).toElement().childNodes();
            QDomElement ref = pos.item(0).toElement();
            float y = ref.attribute("Y", "0").toFloat();
            float x = ref.attribute("X", "0").toFloat();
            float z = ref.attribute("Z", "0").toFloat();
            QDomElement xvect = pos.item(1).toElement();
            float yx = xvect.attribute("Y", "0").toFloat();
            float xx = xvect.attribute("X", "0").toFloat();
            float zx = xvect.attribute("Z", "0").toFloat();
            QDomElement yvect = pos.item(2).toElement();
            float yy = yvect.attribute("Y", "0").toFloat();
            float xy = yvect.attribute("X", "0").toFloat();
            float zy = yvect.attribute("Z", "0").toFloat();
            QVector3D refp,xvector,yvector;
            refp.setX(x);
            refp.setY(y);
            refp.setZ(z);
            xvector.setX(xx);
            xvector.setY(yx);
            xvector.setZ(zx);
            yvector.setX(xy);
            yvector.setY(yy);
            yvector.setZ(zy);
            p.guid=guid;
            p.refPoint = refp;
            p.xvector=xvector;
            p.yvector=yvector;
            transfo.append(p);

            }

            parts.at(i).transformation.append(transfo);

        }
    }
}
void loadrefsiderawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Rawparts/Rawpart/ReferenceSide
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i)
    {
        if (partNodes.item(i).isElement()) {
        QDomElement refside = partNodes.item(i).toElement();
        QString align = refside.attribute("Align", "");
        int side = refside.attribute("Side", "0").toInt();
        parts[i].rf.align = align;
        parts[i].rf.side = side;
        }
    }
}
void loadrefsidepart(QXmlQuery &query,std::vector<Btlx::Part> &parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Parts/Part/ReferenceSide
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i)
    {
        if (partNodes.item(i).isElement()) {
        QDomElement refside = partNodes.item(i).toElement();
        QString align = refside.attribute("Align", "");
        int side = refside.attribute("Side", "0").toInt();
        parts[i].rf.align = align;
        parts[i].rf.side = side;
        }
    }
}
void loadcolorpart(QXmlQuery &query,std::vector<Btlx::Part> &parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Parts/Part/Colour
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i)
    {
        if (partNodes.item(i).isElement()) {
        QDomElement color = partNodes.item(i).toElement();
        int red = color.attribute("Red", "0").toInt();
        int blue = color.attribute("Green", "0").toInt();
        int green = color.attribute("Blue", "0").toInt();
        int t = color.attribute("Transparency", "0").toInt();
        parts[i].color.green = green;
        parts[i].color.blue = blue;
        parts[i].color.red = red;
        parts[i].color.transparency = t;
        }
    }
}
void loadpartrefrawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Rawparts/Rawpart/PartRefs
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i) {
        QList<Btlx::Position> transfo;
        QDomNodeList transfosChildNodes = partNodes.item(i).toElement().childNodes();
        if (partNodes.item(i).isElement()) {
            for(int j =0;j<transfosChildNodes.length();j++){
            QDomElement part = transfosChildNodes.item(j).toElement();
            Btlx::Position p;
            p.type = "Transformation";
            QString guid = part.attribute("GUID", "");
            QDomNodeList transfoo = transfosChildNodes.item(j).toElement().childNodes();
            QDomNodeList pos = transfoo.item(0).toElement().childNodes();
            QDomElement ref = pos.item(0).toElement();
            float y = ref.attribute("Y", "0").toFloat();
            float x = ref.attribute("X", "0").toFloat();
            float z = ref.attribute("Z", "0").toFloat();
            QDomElement xvect = pos.item(1).toElement();
            float yx = xvect.attribute("Y", "0").toFloat();
            float xx = xvect.attribute("X", "0").toFloat();
            float zx = xvect.attribute("Z", "0").toFloat();
            QDomElement yvect = pos.item(2).toElement();
            float yy = yvect.attribute("Y", "0").toFloat();
            float xy = yvect.attribute("X", "0").toFloat();
            float zy = yvect.attribute("Z", "0").toFloat();
            QVector3D refp,xvector,yvector;
            refp.setX(x);
            refp.setY(y);
            refp.setZ(z);
            xvector.setX(xx);
            xvector.setY(yx);
            xvector.setZ(zx);
            yvector.setX(xy);
            yvector.setY(yy);
            yvector.setZ(zy);
            p.guid=guid;
            p.refPoint = refp;
            p.xvector=xvector;
            p.yvector=yvector;
            transfo.append(p);

            }

            parts.at(i).partref.append(transfo);

        }
    }
}
void loadrefplanerawpart(QXmlQuery &query,std::vector<Btlx::RawPart> &parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Rawparts/Rawpart/UserReferencePlanes
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i) {
        QList<Btlx::Position> transfo;
        QDomNodeList transfosChildNodes = partNodes.item(i).toElement().childNodes();
        if (partNodes.item(i).isElement()) {
            for(int j =0;j<transfosChildNodes.length();j++){
            QDomElement part = transfosChildNodes.item(j).toElement();
            Btlx::Position p;
            p.type = "UserReferencePlane";
            QString guid = part.attribute("ID", "");
            QDomNodeList transfoo = transfosChildNodes.item(j).toElement().childNodes();
            QDomNodeList pos = transfoo.item(0).toElement().childNodes();
            QDomElement ref = pos.item(0).toElement();
            float y = ref.attribute("Y", "0").toFloat();
            float x = ref.attribute("X", "0").toFloat();
            float z = ref.attribute("Z", "0").toFloat();
            QDomElement xvect = pos.item(1).toElement();
            float yx = xvect.attribute("Y", "0").toFloat();
            float xx = xvect.attribute("X", "0").toFloat();
            float zx = xvect.attribute("Z", "0").toFloat();
            QDomElement yvect = pos.item(2).toElement();
            float yy = yvect.attribute("Y", "0").toFloat();
            float xy = yvect.attribute("X", "0").toFloat();
            float zy = yvect.attribute("Z", "0").toFloat();
            QVector3D refp,xvector,yvector;
            refp.setX(x);
            refp.setY(y);
            refp.setZ(z);
            xvector.setX(xx);
            xvector.setY(yx);
            xvector.setZ(zx);
            yvector.setX(xy);
            yvector.setY(yy);
            yvector.setZ(zy);
            p.guid=guid;
            p.refPoint = refp;
            p.xvector=xvector;
            p.yvector=yvector;
            transfo.append(p);

            }

            parts.at(i).userrefplane.append(transfo);

        }
    }
}
void loadrefplanepart(QXmlQuery &query,std::vector<Btlx::Part> &parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Parts/Part/UserReferencePlanes
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i) {
        QList<Btlx::Position> transfo;
        QDomNodeList transfosChildNodes = partNodes.item(i).toElement().childNodes();
        if (partNodes.item(i).isElement()) {
            for(int j =0;j<transfosChildNodes.length();j++){
            QDomElement part = transfosChildNodes.item(j).toElement();
            Btlx::Position p;
            p.type = "UserReferencePlane";
            QString guid = part.attribute("ID", "");
            QDomNodeList transfoo = transfosChildNodes.item(j).toElement().childNodes();
            QDomNodeList pos = transfoo.item(0).toElement().childNodes();
            QDomElement ref = pos.item(0).toElement();
            float y = ref.attribute("Y", "0").toFloat();
            float x = ref.attribute("X", "0").toFloat();
            float z = ref.attribute("Z", "0").toFloat();
            QDomElement xvect = pos.item(1).toElement();
            float yx = xvect.attribute("Y", "0").toFloat();
            float xx = xvect.attribute("X", "0").toFloat();
            float zx = xvect.attribute("Z", "0").toFloat();
            QDomElement yvect = pos.item(2).toElement();
            float yy = yvect.attribute("Y", "0").toFloat();
            float xy = yvect.attribute("X", "0").toFloat();
            float zy = yvect.attribute("Z", "0").toFloat();
            QVector3D refp,xvector,yvector;
            refp.setX(x);
            refp.setY(y);
            refp.setZ(z);
            xvector.setX(xx);
            xvector.setY(yx);
            xvector.setZ(zx);
            yvector.setX(xy);
            yvector.setY(yy);
            yvector.setZ(zy);
            p.guid=guid;
            p.refPoint = refp;
            p.xvector=xvector;
            p.yvector=yvector;
            transfo.append(p);

            }

            parts.at(i).userrefplane.append(transfo);

        }
    }
}
void loadtransformationpart(QXmlQuery &query,std::vector<Btlx::Part> &parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Parts/Part/Transformations
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i) {
        QList<Btlx::Position> transfo;
        QDomNodeList transfosChildNodes = partNodes.item(i).toElement().childNodes();
        if (partNodes.item(i).isElement()) {
            for(int j =0;j<transfosChildNodes.length();j++){
            QDomElement part = transfosChildNodes.item(j).toElement();
            Btlx::Position p;
            p.type = "Transformation";
            QString guid = part.attribute("GUID", "");
            QDomNodeList transfoo = transfosChildNodes.item(j).toElement().childNodes();
            QDomNodeList pos = transfoo.item(0).toElement().childNodes();
            QDomElement ref = pos.item(0).toElement();
            float y = ref.attribute("Y", "0").toFloat();
            float x = ref.attribute("X", "0").toFloat();
            float z = ref.attribute("Z", "0").toFloat();
            QDomElement xvect = pos.item(1).toElement();
            float yx = xvect.attribute("Y", "0").toFloat();
            float xx = xvect.attribute("X", "0").toFloat();
            float zx = xvect.attribute("Z", "0").toFloat();
            QDomElement yvect = pos.item(2).toElement();
            float yy = yvect.attribute("Y", "0").toFloat();
            float xy = yvect.attribute("X", "0").toFloat();
            float zy = yvect.attribute("Z", "0").toFloat();
            QVector3D refp,xvector,yvector;
            refp.setX(x);
            refp.setY(y);
            refp.setZ(z);
            xvector.setX(xx);
            xvector.setY(yx);
            xvector.setZ(zx);
            yvector.setX(xy);
            yvector.setY(yy);
            yvector.setZ(zy);
            p.guid=guid;
            p.refPoint = refp;
            p.xvector=xvector;
            p.yvector=yvector;
            transfo.append(p);

            }

            parts.at(i).transformation.append(transfo);

        }
    }
}
void loadrawpartshape(QXmlQuery& query, std::vector<Btlx::RawPart>& parts)
{
    query.setQuery(R"(
                       declare default element namespace "http://www.design2machine.com";
                       /BTLx/Project/Rawparts/Rawpart/Shape
                       )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i) {
        QList<QList<int>> faces;
        QDomNodeList shapeChildNodes = partNodes.item(i).toElement().childNodes();
        if (shapeChildNodes.item(0).isElement()) {
            QDomElement indexedFaceSetNode = shapeChildNodes.item(0).toElement();
            QString coords = indexedFaceSetNode.attribute("coordIndex", "None").trimmed();

            QStringList faceStrings = coords.split("-1");
            for (int j = 0; j < faceStrings.size(); j++) {
                QString faceString = faceStrings.at(j).trimmed();
                if (faceString.isEmpty()) {
                    continue;
                }

                QStringList indicesStrings = faceString.split(" ");
                QList<int> face;
                for (int k = 0; k < indicesStrings.size(); k++) {
                    QString indexString = indicesStrings.at(k).trimmed();
                    face.append(indexString.toInt());
                }
                faces.append(face);
            }
            parts[i].indexCoor = faces;
        }

        shapeChildNodes = shapeChildNodes.item(0).toElement().childNodes();
        if (shapeChildNodes.item(0).isElement()) {
            QDomElement part = shapeChildNodes.item(0).toElement();
            QString pointsString = part.attribute("point", "").trimmed();
            QStringList coordsStrings = pointsString.split(" ");

            if (coordsStrings.size() % 3 != 0) {
                throw std::runtime_error{"Point list size (in a shape) must be a multiple of 3"};
            }

            QList<QVector3D> points;
            for (int k = 0; k + 2 < coordsStrings.size(); k += 3) {
                float x = coordsStrings.at(k).toFloat();
                float y = coordsStrings.at(k + 1).toFloat();
                float z = coordsStrings.at(k + 2).toFloat();
                QVector3D point;
                point.setX(x);
                point.setY(y);
                point.setZ(z);
                points.append(point);
            }
            parts[i].points = std::move(points);
        }
    }
}

void loadrawparts(QXmlQuery& query, std::vector<Btlx::RawPart>& rparties)
{
    query.setQuery(R"(
                           declare default element namespace "http://www.design2machine.com";
                           /BTLx/Project/Rawparts/Rawpart
                           )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    rparties.reserve(rparties.size() + partNodes.length());
    for (int i = 0; i < partNodes.length(); ++i) {
        if (partNodes.item(i).isElement()) {
            QDomElement part = partNodes.item(i).toElement();
            int number = part.attribute("SingleMemberNumber", "0").toInt();
            int enumber = part.attribute("ElementNumber", "0").toInt();
            int count = part.attribute("Count", "0").toInt();
            QString material = part.attribute("Material", "No material");
            float w = part.attribute("Width", "0").toFloat();
            float h = part.attribute("Height", "0").toFloat();
            float l = part.attribute("Length", "0").toFloat();
            QString nom = part.attribute("Designation", "No name");
            Btlx::RawPart rpartie;
            rpartie.count = count;
            rpartie.elementnumber = enumber;
            rpartie.material = material;
            rpartie.number = number;
            rpartie.designation = nom;
            rpartie.size = QVector3D(l, h, w);
            rparties.push_back(std::move(rpartie));
        }
    }
    loadrawpartshape(query, rparties);
    loadtransformationrawpart(query, rparties);
    loadrefplanerawpart(query,rparties);
    loadpartrefrawpart(query,rparties);
    loadrefsiderawpart(query,rparties);
}
void loadpartshape(QXmlQuery& query, std::vector<Btlx::Part>& parts)
{
    query.setQuery(R"(
                           declare default element namespace "http://www.design2machine.com";
                           /BTLx/Project/Parts/Part/Shape
                           )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    for (int i = 0; i < partNodes.length(); ++i) {
        QList<QList<int>> faces;
        QDomNodeList shapeChildNodes = partNodes.item(i).toElement().childNodes();
        if (shapeChildNodes.item(0).isElement()) {
            QDomElement indexedFaceSetNode = shapeChildNodes.item(0).toElement();
            QString coords = indexedFaceSetNode.attribute("coordIndex", "None").trimmed();

            QStringList faceStrings = coords.split("-1");
            for (int j = 0; j < faceStrings.size(); j++) {
                QString faceString = faceStrings.at(j).trimmed();
                if (faceString.isEmpty()) {
                    continue;
                }

                QStringList indicesStrings = faceString.split(" ");
                QList<int> face;
                for (int k = 0; k < indicesStrings.size(); k++) {
                    QString indexString = indicesStrings.at(k).trimmed();
                    face.append(indexString.toInt());
                }
                faces.append(face);
            }
            parts[i].indexCoor = faces;
        }

        shapeChildNodes = shapeChildNodes.item(0).toElement().childNodes();
        if (shapeChildNodes.item(0).isElement()) {
            QDomElement part = shapeChildNodes.item(0).toElement();
            QString pointsString = part.attribute("point", "").trimmed();
            QStringList coordsStrings = pointsString.split(" ");

            if (coordsStrings.size() % 3 != 0) {
                throw std::runtime_error{"Point list size (in a shape) must be a multiple of 3"};
            }

            QList<QVector3D> points;
            for (int k = 0; k + 2 < coordsStrings.size(); k += 3) {
                float x = coordsStrings.at(k).toFloat();
                float y = coordsStrings.at(k + 1).toFloat();
                float z = coordsStrings.at(k + 2).toFloat();
                QVector3D point;
                point.setX(x);
                point.setY(y);
                point.setZ(z);
                points.append(point);
            }
            parts[i].points = std::move(points);
        }
    }
}
void loadparts(QXmlQuery& query, std::vector<Btlx::Part>& parties)
{
    query.setQuery(R"(
                           declare default element namespace "http://www.design2machine.com";
                           /BTLx/Project/Parts/Part
                           )");
    QString res;
    QDomDocument partDocument;
    query.evaluateTo(&res);
    partDocument.setContent("<a>" + res + "</a>");
    QDomNodeList partNodes = partDocument.documentElement().childNodes();
    parties.reserve(parties.size() + partNodes.length());
    for (int i = 0; i < partNodes.length(); ++i) {
        if (partNodes.item(i).isElement()) {
            QDomElement part = partNodes.item(i).toElement();
            int number = part.attribute("SingleMemberNumber", "0").toInt();
            int enumber = part.attribute("ElementNumber", "0").toInt();
            int count = part.attribute("Count", "0").toInt();
            QString material = part.attribute("Material", "No material");
            float w = part.attribute("Width", "0").toFloat();
            float h = part.attribute("Height", "0").toFloat();
            float l = part.attribute("Length", "0").toFloat();
            QString nom = part.attribute("Designation", "No name");
            int onumber = part.attribute("OrderNumber", "-1").toInt();
            int assembly = part.attribute("AssemblyNumber", "-1").toInt();
            QString group = part.attribute("Group", "No name");
            QString storey = part.attribute("Storey", "No name");
            Btlx::Part partie;
            partie.assemblynumber = assembly;
            partie.ordernumber = onumber;
            partie.group = group;
            partie.storey = storey;
            partie.count = count;
            partie.elementnumber = enumber;
            partie.material = material;
            partie.number = number;
            partie.designation = nom;
            partie.size = QVector3D(l, h, w);
            parties.push_back(std::move(partie));
        }
    }
    loadpartshape(query, parties);
    loadtransformationpart(query,parties);
    loadrefplanepart(query,parties);
    loadrefsidepart(query,parties);
    loadcolorpart(query,parties);
}

void computeLinks(Btlx::Project& project)
{
    // map GUID and parts
    std::unordered_map<QString, std::size_t> links;
    for (std::size_t i = 0; i < project.parts.size(); ++i) {
        for (const auto& transformation : project.parts[i].transformation) {
            links.emplace(transformation.guid, i);
        }
    }

    // links indices and GUID
    for (auto& rpart : project.rparts) {
        rpart.links.reserve(rpart.partref.size());
        for (int j = 0; j < rpart.partref.size(); ++j) {
            auto it = links.find(rpart.partref[j].guid);
            if (it == links.end()) {
                throw std::runtime_error{"GUID manquant"};
            }

            rpart.links.push_back(Btlx::PartLink{
                it->second,
                static_cast<std::size_t>(j)
            });
        }
    }
}

Btlx::Project loadfile(const QUrl& url)
{
    QFile btlxFile(url.toLocalFile());
    if (!btlxFile.open(QIODevice::ReadOnly)) {
        throw std::runtime_error{"Ouverture du document impossible"};
    }

    QByteArray btlxData(btlxFile.readAll());
    btlxFile.close();

    Btlx::Project project;
    project.domDoc.setContent(btlxData);

    QXmlQuery queryPart;
    queryPart.setFocus(url);
    loadparts(queryPart, project.parts);
    loadrawparts(queryPart, project.rparts);
    computeLinks(project);
    return project;
}
} //namespace load

namespace save {

namespace {

template<typename Container, typename Func>
void updateXmlList(QDomElement& elt, const QString& tagName, const Container& c, Func updateEach)
{
    QDomNodeList nodeList = elt.childNodes();

    typename Container::size_type containerIndex = 0;
    for (int i = 0, count = nodeList.count(); i < count; ++i) {
        QDomNode rawpartNode = nodeList.item(i);

        if (!rawpartNode.isElement()) {
            continue;
        }

        QDomElement element = rawpartNode.toElement();

        if (element.tagName() != tagName) {
            continue;
        }

        if (containerIndex >= c.size()) {
            throw std::runtime_error{"Erreur sauvegarde : taille du XML invalide"};
        }

        const auto& data = c[containerIndex];

        updateEach(data, element);

        ++containerIndex;
    }
}

void updateVectorXml(const QVector3D& vec, QDomElement& xml)
{
    xml.setAttribute("X", QString::number(vec.x()));
    xml.setAttribute("Y", QString::number(vec.y()));
    xml.setAttribute("Z", QString::number(vec.z()));
}

template<typename PartBase>
void updateShapeXml(const PartBase& p, QDomElement& xml)
{
    QDomElement indexedFaceSetXml = xml.firstChildElement("IndexedFaceSet");
    indexedFaceSetXml.setAttribute("convex", "false");
    QString res = "";
    for (const auto& coord : p.indexCoor) {
        for (const auto& index : coord) {
            res += QString::number(index);
            res += " ";
        }
        res += "-1 ";
    }
    indexedFaceSetXml.setAttribute("coordIndex", res);

    QDomElement coordinateXml = indexedFaceSetXml.firstChildElement("Coordinate");
    res = "";
    for (const auto& point : p.points) {
        res += QString::number(point.x()) + " ";
        res += QString::number(point.y()) + " ";
        res += QString::number(point.z()) + " ";
    }
    coordinateXml.setAttribute("point", res);
}

void updateTransformationXml(const Btlx::Position& transformation, QDomElement& xml, const QString& idName)
{
    xml.setAttribute(idName, transformation.guid);

    QDomElement positionXml = xml.firstChildElement("Position");

    QDomElement refPointXml = positionXml.firstChildElement("ReferencePoint");
    updateVectorXml(transformation.refPoint, refPointXml);

    QDomElement xVectorXml = positionXml.firstChildElement("XVector");
    updateVectorXml(transformation.xvector, xVectorXml);

    QDomElement yVectorXml = positionXml.firstChildElement("YVector");
    updateVectorXml(transformation.yvector, yVectorXml);
}

inline void updateTransformationsXml(const QList<Btlx::Position>& transformations, QDomElement& xml, const QString& tagName, const QString& idName)
{
    updateXmlList(xml, tagName, transformations, [&idName] (const auto& p1, auto& p2) {
        updateTransformationXml(p1, p2, idName);
    });
}

template<typename PartBase>
void updateGenericPart(const PartBase& part, QDomElement& xml)
{
    xml.setAttribute("OrderNumber", QString::number(part.number));
    xml.setAttribute("Height", QString::number(part.size.y()));
    xml.setAttribute("Length", QString::number(part.size.x()));
    xml.setAttribute("Width", QString::number(part.size.z()));
    xml.setAttribute("Material", part.material);
    xml.setAttribute("Designation", part.designation);
    xml.setAttribute("ElementNumber", QString::number(part.elementnumber));
    xml.setAttribute("SingleMemberNumber", QString::number(part.number));
    xml.setAttribute("Count", QString::number(part.count));

    QDomElement transformationsXml = xml.firstChildElement("Transformations");
    updateTransformationsXml(part.transformation, transformationsXml, "Transformation", "GUID");

    QDomElement refSideXml = xml.firstChildElement("ReferenceSide");
    refSideXml.setAttribute("Align", part.rf.align);
    refSideXml.setAttribute("Side", QString::number(part.rf.side));

    QDomElement userRefPlanesXml = xml.firstChildElement("UserReferencePlanes");
    updateTransformationsXml(part.userrefplane, userRefPlanesXml, "UserReferencePlane", "ID");

    QDomElement shapeXml = xml.firstChildElement("Shape");
    updateShapeXml(part, shapeXml);
}

void updateRawPartXml(const Btlx::RawPart& rawpart, QDomElement& rawpartXml)
{
    updateGenericPart(rawpart, rawpartXml);

    QDomElement partRefXml = rawpartXml.firstChildElement("PartRefs");
    updateTransformationsXml(rawpart.partref, partRefXml, "PartRef", "GUID");
}

inline void updateRawPartsXml(const Btlx::Project& project, QDomElement& rawparts)
{
    updateXmlList(rawparts, "Rawpart", project.rparts, updateRawPartXml);
}

void updatePartXml(const Btlx::Part& part, QDomElement& partXml)
{
    updateGenericPart(part, partXml);

    partXml.setAttribute("AssemblyNumber", QString::number(part.assemblynumber));
    partXml.setAttribute("Group", part.group);
    partXml.setAttribute("Storey", part.storey);

    QDomElement colourXml = partXml.firstChildElement("Colour");
    colourXml.setAttribute("Blue", QString::number(part.color.blue));
    colourXml.setAttribute("Green", QString::number(part.color.green));
    colourXml.setAttribute("Transparency", QString::number(part.color.transparency));
    colourXml.setAttribute("Red", QString::number(part.color.red));
}

inline void updatePartsXml(const Btlx::Project& project, QDomElement& parts)
{
    updateXmlList(parts, "Part", project.parts, updatePartXml);
}

} // namespace

void saveProject(Btlx::Project& project, QFile& file)
{
    qSetGlobalQHashSeed(0);
    QDomElement root = project.domDoc.documentElement();
    QDomElement projectXml = root.firstChildElement("Project");

    QDomElement rawparts = projectXml.firstChildElement("Rawparts");
    updateRawPartsXml(project, rawparts);

    QDomElement parts = projectXml.firstChildElement("Parts");
    updatePartsXml(project, parts);

    QTextStream stream(&file);
    project.domDoc.save(stream, 3);
    qSetGlobalQHashSeed(-1);
}

} // namespace save
