#include "geometryengine.hpp"

#include <algorithm>
#include <array>
#include <iterator>

#define GEOMETRY_SHADER_ATTRIBUTE(name, dataPodType, dataPodMember, oglType, count) \
    (shaderAttribute((name), offsetof(dataPodType, dataPodMember), (oglType), (count), sizeof(dataPodType)))

namespace {

struct CubeVertexData {
    QVector3D position;
    QVector3D normal;
};

struct AxesVertexData {
    QVector3D position;
    QVector3D color;
};

struct OverlayData {
    QVector3D position;
    QVector2D texCoord;
};

} // namespace

AxesGeometry::AxesGeometry(QOpenGLWidget& widget):
    Geometry(widget, "axes", false)
{
    std::array<AxesVertexData, vertexCount> vertices{{
        {{0.F, 0.F, 0.F}, {1.F, 0.F, 0.F}},
        {{1.F, 0.F, 0.F}, {1.F, 0.F, 0.F}},

        {{0.F, 0.F, 0.F}, {0.F, 1.F, 0.F}},
        {{0.F, 1.F, 0.F}, {0.F, 1.F, 0.F}},

        {{0.F, 0.F, 0.F}, {0.F, 0.F, 1.F}},
        {{0.F, 0.F, 1.F}, {0.F, 0.F, 1.F}},
    }};

    vboAllocate(vertices);
}

void AxesGeometry::draw(const QQuaternion& rotation, float aspect)
{
    beginDraw();

    QMatrix4x4 projection;
    projection.ortho(-1.F, 1.F, -1.F, 1.F, 0.F, 2.F);
    projection.translate(-.8F, .8F);
    projection.scale(.1F / aspect, .1F, .1F);
    shaderUniform("u_projection", projection);

    QMatrix4x4 view;
    view.translate(0.F, 0.F, -1.F);
    view.rotate(rotation.conjugated());
    shaderUniform("u_view", view);

    GEOMETRY_SHADER_ATTRIBUTE("a_position", AxesVertexData, position, GL_FLOAT, 3);
    GEOMETRY_SHADER_ATTRIBUTE("a_color", AxesVertexData, color, GL_FLOAT, 3);

    float lineWidth;
    glGetFloatv(GL_LINE_WIDTH, &lineWidth);
    glLineWidth(3.F);
    glDrawArrays(GL_LINES, 0, vertexCount);
    glLineWidth(lineWidth);

    endDraw();
}

Geometry::Geometry(QOpenGLWidget& widget, const QString& shaderName, bool ebo):
    m_widget{&widget},
    m_ebo{ebo},
    m_indexBuf{QOpenGLBuffer::IndexBuffer}
{
    m_widget->makeCurrent();

    if (!m_program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/" + shaderName + ".vsh")) {
        throw std::runtime_error(("Vertex shader `" + shaderName + ".vsh` failed to compile:\n" + m_program.log()).toStdString());
    }

    if (!m_program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/" + shaderName + ".fsh")) {
        throw std::runtime_error(("Fragment shader `sol" + shaderName + ".fsh` failed to compile:\n" + m_program.log()).toStdString());
    }

    if (!m_program.link()) {
        throw std::runtime_error("Shader program failed to link:\n" + m_program.log().toStdString());
    }

    initializeOpenGLFunctions();

    if (!m_arrayBuf.create()) {
        throw std::runtime_error("Vertex Buffer Objects are not supported");
    }

    if (m_ebo) {
        if (!m_indexBuf.create()) {
            throw std::runtime_error("Element Buffer Objects are not supported");
        }
    }

    m_widget->doneCurrent();
}

Geometry::~Geometry() noexcept
{
    m_widget->makeCurrent();
    m_arrayBuf.destroy();
    if (m_ebo) {
        m_indexBuf.destroy();
    }
    m_widget->doneCurrent();
}

void Geometry::shaderAttribute(const char* name, size_t offset, GLenum type, size_t count, size_t stride)
{
    int attrLocation = m_program.attributeLocation(name);
    m_program.enableAttributeArray(attrLocation);
    m_program.setAttributeBuffer(attrLocation, type, static_cast<int>(offset), static_cast<int>(count), static_cast<int>(stride));
}

void Geometry::shaderAttribute(const char* name, GLenum type, std::size_t count)
{
    int attrLocation = m_program.attributeLocation(name);
    m_program.enableAttributeArray(attrLocation);
    m_program.setAttributeBuffer(attrLocation, type, 0, static_cast<int>(count));
}

void ShapesGeometry::clear()
{
    m_points = std::vector<QVector3D>{};
    m_indices = std::vector<unsigned>{};
    m_parts = std::vector<PartElement>{};
    m_rawParts = std::vector<RawPartElement>{};
}

std::vector<std::size_t> ShapesGeometry::getShape(const QList<QList<int>>& indices, const QList<QVector3D>& points)
{
    std::vector<std::size_t> shape;
    shape.reserve(indices.size() + 1);

    shape.push_back(m_indices.size());
    for (const auto& faceIndices : indices) {
        addFace(faceIndices, shape);
    }

    m_points.reserve(m_points.size() + points.size());
    std::copy(points.begin(), points.end(), std::back_inserter(m_points));
    return shape;
}

void ShapesGeometry::setProject(const Btlx::Project& project)
{
    m_project = &project;

    updateGeometry();
}

void ShapesGeometry::updateGeometry()
{
    Q_ASSERT(m_project != nullptr);

    clear();
    m_parts.reserve(m_project->parts.size());
    for (const auto& part : m_project->parts) {
        auto shape = getShape(part.indexCoor, part.points);
        std::vector<QMatrix4x4> transforms;
        transforms.reserve(part.transformation.size());
        for (const auto& t : part.transformation) {
            transforms.push_back(t.getTransform());
        }
        m_parts.push_back(PartElement{std::move(shape), std::move(transforms)});
    }

    m_rawParts.reserve(m_project->rparts.size());
    for (const auto& rpart : m_project->rparts) {
        auto shape = getShape(rpart.indexCoor, rpart.points);
        std::vector<LinkElement> links;
        links.reserve(rpart.links.size());
        for (const auto& l : rpart.links) {
            links.push_back(LinkElement{
                l.partIndex,
                rpart.partref[static_cast<int>(l.rpartTransformIndex)].getTransform()
            });
        }
        m_rawParts.push_back(RawPartElement{std::move(shape), std::move(links)});
    }

    vboAllocate(m_points);
    eboAllocate(m_indices);
}

void ShapesGeometry::updateGlobalTransform(std::size_t index, std::size_t transformIndex)
{
    Q_ASSERT(m_project != nullptr);

    Q_ASSERT(m_parts.size() == m_project->parts.size());
    Q_ASSERT(index < m_parts.size());
    Q_ASSERT(transformIndex < m_parts[index].tranforms.size());

    m_parts[index].tranforms[transformIndex] = m_project->parts[index].transformation[static_cast<int>(transformIndex)].getTransform();
}

void ShapesGeometry::updateRawPartTransform(std::size_t index, std::size_t transformIndex)
{
    Q_ASSERT(m_project != nullptr);

    Q_ASSERT(m_rawParts.size() == m_project->rparts.size());
    Q_ASSERT(index < m_rawParts.size());
    Q_ASSERT(transformIndex < m_rawParts[index].links.size());

    m_rawParts[index].links[transformIndex].transform = m_project->rparts[index].partref[static_cast<int>(transformIndex)].getTransform();
}

void ShapesGeometry::drawProject(const QMatrix4x4& view, const QMatrix4x4& projection)
{
    beginDraw();

    shaderUniform("u_projection", projection);
    shaderUniform("u_view", view);
    shaderAttribute("a_position", GL_FLOAT, 3);

    for (const auto& shape : m_parts) {
        for (const auto& t : shape.tranforms) {
            shaderUniform("u_model", t);
            drawShape(shape.faces);
        }
    }

    endDraw();
}

void ShapesGeometry::drawPart(std::size_t index, const QMatrix4x4& view, const QMatrix4x4& projection)
{
    beginDraw();

    const PartElement& elt = m_parts[index];

    shaderUniform("u_projection", projection);
    shaderUniform("u_view", view);
    shaderUniform("u_model", QMatrix4x4{});
    shaderAttribute("a_position", GL_FLOAT, 3);

    drawShape(elt.faces);

    endDraw();
}

void ShapesGeometry::drawRawPart(std::size_t index, const QMatrix4x4& view, const QMatrix4x4& projection, std::ptrdiff_t transformIndex)
{
    beginDraw();

    const RawPartElement& elt = m_rawParts[index];

    shaderUniform("u_projection", projection);
    shaderUniform("u_view", view);
    shaderUniform("u_model", QMatrix4x4{});
    shaderAttribute("a_position", GL_FLOAT, 3);

    drawShape(elt.faces);

    for (std::size_t i = 0; i < elt.links.size(); ++i) {
        const auto& link = elt.links[i];
        shaderUniform("u_model", link.transform);
        drawShape(m_parts[link.index].faces, static_cast<std::size_t>(transformIndex) == i);
    }

    endDraw();
}

void ShapesGeometry::addFace(const QList<int>& indices, std::vector<std::size_t>& shapeFaces)
{
    if (indices.size() < 3) {
        throw std::runtime_error("A face must at least be a triangle");
    }

    unsigned baseIndex = static_cast<unsigned>(m_points.size());
    std::size_t firstNewIndex = m_indices.size();

    m_indices.reserve(m_indices.size() + indices.size());
    std::copy(indices.begin(), indices.end(), std::back_inserter(m_indices));

    for (std::size_t i = firstNewIndex; i < m_indices.size(); ++i) {
        m_indices[i] += baseIndex;
    }

    shapeFaces.push_back(m_indices.size());
}

void ShapesGeometry::drawShape(const std::vector<size_t>& shape, bool highlight)
{
    if (highlight) {
        shaderUniform("u_color", QVector3D(1., 0., 1.));
    } else {
        shaderUniform("u_color", QVector3D(0., 0., 0.));
    }

    for (std::size_t i = 0; i + 1 < shape.size(); ++i) {
        float lineWidth;
        glGetFloatv(GL_LINE_WIDTH, &lineWidth);
        if (highlight) {
            glLineWidth(2.F);
        }

        const auto& first = shape[i];
        const auto& next = shape[i + 1];
        glDrawElements(GL_LINE_LOOP, static_cast<GLsizei>(next - first), GL_UNSIGNED_INT, reinterpret_cast<void*>(first * sizeof(unsigned)));

        if (highlight) {
            glLineWidth(lineWidth);
        }
    }
}

OverlayGeometry::OverlayGeometry(QOpenGLWidget& widget):
    Geometry{widget, "overlay", false},
    m_texture{nullptr}
{
    std::array<OverlayData, vertexCount> vertices{{
        {{-1.F, -1.F, 0.F}, {0.F, 0.F}},
        {{ 1.F, -1.F, 0.F}, {1.F, 0.F}},
        {{-1.F,  1.F, 0.F}, {0.F, 1.F}},
        {{ 1.F,  1.F, 0.F}, {1.F, 1.F}}
    }};

    vboAllocate(vertices);
}

OverlayGeometry::~OverlayGeometry() noexcept
{
    widget().makeCurrent();

    delete m_texture;

    widget().doneCurrent();
}

void OverlayGeometry::draw()
{
    if (m_texture == nullptr) {
        return;
    }

    beginDraw();

    m_texture->bind();

    shaderUniform("u_texture", 0);

    GEOMETRY_SHADER_ATTRIBUTE("a_position", OverlayData, position, GL_FLOAT, 3);
    GEOMETRY_SHADER_ATTRIBUTE("a_tex_coord", OverlayData, texCoord, GL_FLOAT, 2);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, vertexCount);

    m_texture->release();

    endDraw();
}

void OverlayGeometry::setOverlay(QImage&& overlay)
{
    widget().makeCurrent();

    delete m_texture;


    m_texture = new QOpenGLTexture(overlay.mirrored());
    m_texture->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
    widget().doneCurrent();
}
