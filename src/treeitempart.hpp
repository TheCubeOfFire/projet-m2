#ifndef TREEITEMPART_H
#define TREEITEMPART_H

#include "btlx.hpp"

#include <QTreeWidgetItem>

#include <cstddef>

class TreeItemPart: public QTreeWidgetItem {
public:
    explicit TreeItemPart(QTreeWidget* parent);
    explicit TreeItemPart(QTreeWidgetItem* parent, std::size_t index, Btlx::ItemType type, std::size_t transformIndex);

    std::size_t getIndex() const;
    Btlx::ItemType getType() const;
    std::size_t getTransformIndex() const;

private:
    std::size_t m_index;
    Btlx::ItemType m_type;
    std::size_t m_transformIndex;
};

#endif // TREEITEMPART_H
