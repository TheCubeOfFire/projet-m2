#include "openglwidget.hpp"

#include <QImage>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QMouseEvent>
#include <QOpenGLContext>
#include <QPainter>
#include <QStringBuilder>
#include <QSurfaceFormat>
#include <QVector4D>

#include <cmath>

namespace {

constexpr float viewDistance = 1E3F;
constexpr float clipDistance = 1E-3F;

void handleDebugMessage(const QOpenGLDebugMessage& message)
{
    qDebug() << "[OpenGL] " << message;
}

template<typename PartBase>
QVector3D sumPoint(const PartBase& p, int& count, const QMatrix4x4& transform = QMatrix4x4{})
{
    QVector3D res;
    for (const auto& face : p.indexCoor) {
        for (const auto& i : face) {
            res += transform.map(p.points[i]);
            ++count;
        }
    }
    return res;
}

} // namespace

OpenGLWidget::OpenGLWidget(const Btlx::Project& project, QWidget* parent):
    QOpenGLWidget(parent),
    m_project(&project),
    m_drawMode(Btlx::ItemType::Project)
{
    QSurfaceFormat format;
    format.setSamples(16);
    format.setOption(QSurfaceFormat::DebugContext);

    setFormat(format);
}

OpenGLWidget::~OpenGLWidget() noexcept
{
    makeCurrent();
    m_logger.stopLogging();
    doneCurrent();
}

void OpenGLWidget::initializeGL()
{
    auto* ctx = QOpenGLContext::currentContext();
    auto* gl = ctx->functions();

    gl->glEnable(GL_DEPTH_TEST);
    gl->glEnable(GL_CULL_FACE);
    gl->glEnable(GL_BLEND);

    m_logger.initialize();
    m_logger.disableMessages(QOpenGLDebugMessage::AnySource, QOpenGLDebugMessage::AnyType, QOpenGLDebugMessage::NotificationSeverity);
    connect(&m_logger, &QOpenGLDebugLogger::messageLogged, &handleDebugMessage);
    m_logger.startLogging();

    m_geometries = std::make_unique<GeometryEngine>(*this);

    m_timer.start(12, this);
}

void OpenGLWidget::resizeGL(int w, int h)
{
    auto* gl = QOpenGLContext::currentContext()->functions();
    gl->glViewport(0, 0, w, h);

    m_aspect = static_cast<float>(w) / static_cast<float>((h != 0) ? h : 1);
}

void OpenGLWidget::paintGL()
{
    auto* gl = QOpenGLContext::currentContext()->functions();
    gl->glClearColor(1.F, 1.F, 1.F, 1.F);
    gl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    QMatrix4x4 projection;
    if (m_aspect > 1.F) {
        projection.ortho(-1.F * m_aspect, 1.F * m_aspect, -1.F, 1.F, clipDistance, viewDistance);
    } else {
        projection.ortho(-1.F, 1.F, -1.F / m_aspect, 1.F / m_aspect, clipDistance, viewDistance);
    }

    QMatrix4x4 viewMat = m_camera.getViewMatrix();

    m_geometries->axes.draw(m_camera.getQuaternion(), m_aspect);

    draw(viewMat, projection);

    gl->glDisable(GL_DEPTH_TEST);
    m_geometries->overlay.draw();
    gl->glEnable(GL_DEPTH_TEST);
}

QVector3D OpenGLWidget::meanPoint() const
{
    QVector3D res;
    int count = 0;

    switch (m_drawMode) {
    case Btlx::ItemType::Project:
        for (const auto& part : m_project->parts) {
            for (const auto& transfomation : part.transformation) {
                QMatrix4x4 modelMat = transfomation.getTransform();
                res += sumPoint(part, count, modelMat);
            }
        }
        break;

    case Btlx::ItemType::RawPart:
    case Btlx::ItemType::PartLink: {
        const Btlx::RawPart& p = m_project->rparts[m_rawPartIndex];
        res = sumPoint(p, count);
        break;
    }

    case Btlx::ItemType::Part: {
        const Btlx::Part& p = m_project->parts[m_partIndex];
        res = sumPoint(p, count);
        break;
    }

    }

    return res * GeometryEngine::unitLength / count;
}

void OpenGLWidget::draw(const QMatrix4x4& view, const QMatrix4x4& projection)
{
    switch (m_drawMode) {
    case Btlx::ItemType::Project:
        m_geometries->shapes.drawProject(view, projection);
        break;

    case Btlx::ItemType::RawPart:
        m_geometries->shapes.drawRawPart(m_rawPartIndex, view, projection);
        break;

    case Btlx::ItemType::PartLink:
        m_geometries->shapes.drawRawPart(m_rawPartIndex, view, projection, m_partIndex);
        break;

    case Btlx::ItemType::Part:
        m_geometries->shapes.drawPart(m_partIndex, view, projection);
        break;
    }
}

void OpenGLWidget::setToFrontView()
{
    m_camera.reset(meanPoint());
}

void OpenGLWidget::setToRightView()
{
    m_camera.reset(meanPoint());
    m_camera.setAngle(QVector2D(0.F, 90.F));
}

void OpenGLWidget::setToUpView()
{
    m_camera.reset(meanPoint());
    m_camera.setAngle(QVector2D(-90.F, 0.F));
}

void OpenGLWidget::updateGeometry()
{
    m_geometries->shapes.setProject(*m_project);
    setToProjectView();
}

void OpenGLWidget::updateGlobalTransform(std::size_t index, std::size_t transformIndex)
{
    m_geometries->shapes.updateGlobalTransform(index, transformIndex);
}

void OpenGLWidget::updateRawPartTransform(std::size_t index, std::size_t transformIndex)
{
    m_geometries->shapes.updateRawPartTransform(index, transformIndex);
}

void OpenGLWidget::setToProjectView()
{
    m_drawMode = Btlx::ItemType::Project;
    setToFrontView();
}

void OpenGLWidget::setToRawPartView(size_t index)
{
    m_drawMode = Btlx::ItemType::RawPart;
    m_rawPartIndex = index;
    setToFrontView();
}

void OpenGLWidget::setToLinkView(size_t rawPartIndex, size_t partIndex)
{
    m_drawMode = Btlx::ItemType::PartLink;
    m_rawPartIndex = rawPartIndex;
    m_partIndex = partIndex;
    setToFrontView();
}

void OpenGLWidget::setToPartView(size_t index)
{
    m_drawMode = Btlx::ItemType::Part;
    m_partIndex = index;
    setToFrontView();
}

void OpenGLWidget::timerEvent(QTimerEvent* /*event*/)
{
    float zoomLevel = m_camera.getZoomLevel();
    double xscale = std::max(m_aspect, 1.F) / zoomLevel * 2.F;
    double yscale = 2. / (zoomLevel * std::min(m_aspect, 1.F));
    QString unit = " m";

    if (xscale < 1. && yscale < 1.) {
        xscale *= 1000.;
        yscale *= 1000.;
        unit = " mm";
    }
    QString text = QString("Échelle : ")
                   % QString::number(xscale, 'G', 3)
                   % QString(" × ")
                   % QString::number(yscale, 'G', 3)
                   % unit;

    QImage image(width(), height(), QImage::Format_RGBA64);
    image.fill(Qt::transparent);

    QPainter painter(&image);
    painter.setPen(QPen(Qt::darkGray));
    auto font = painter.font();
    font.setPointSize(12);
    font.setBold(true);
    painter.setFont(font);
    painter.drawText(0, 0, image.width() - 5, image.height() - 5, Qt::AlignBottom | Qt::AlignRight, text);
    m_geometries->overlay.setOverlay(std::move(image));

    m_camera.update();
    update();
}

void OpenGLWidget::mousePressEvent(QMouseEvent* event)
{
    m_mouseLastPosition = QVector2D(event->localPos());
}

void OpenGLWidget::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::MouseButton::LeftButton) {
        m_camera.setAngleSpeed(QVector2D());
    } else if (event->button() == Qt::MouseButton::RightButton) {
        m_camera.setMoveSpeed(QVector2D());
    }
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent* event)
{
    bool leftPressed = (event->buttons() & Qt::MouseButton::LeftButton) != 0;
    bool rightPressed = (event->buttons() & Qt::MouseButton::RightButton) != 0;

    if (leftPressed && rightPressed) {
        return;
    }

    QVector2D currentPos(event->localPos());
    auto diff = currentPos - m_mouseLastPosition;

    if (leftPressed) {
        m_camera.setAngleSpeed(QVector2D(-diff.y(), -diff.x()));
    }

    if (rightPressed) {
        constexpr float mouseSpeedFactor = 4E-3F;
        m_camera.setMoveSpeed(QVector2D(-diff.x(), diff.y()) * mouseSpeedFactor);
    }

    m_mouseLastPosition = currentPos;
}

void OpenGLWidget::wheelEvent(QWheelEvent* event)
{
    constexpr float mouseWheelStepAngle = 120.F;
    constexpr float mouseZoomSrength = .5F;
    m_camera.zoomIn(static_cast<float>(event->angleDelta().y()) / mouseWheelStepAngle * mouseZoomSrength);

    update();
}

void OpenGLCamera::reset(const QVector3D& origin)
{
    m_angle = QVector2D();
    m_angleSpeed = QVector2D();

    m_track = origin;
    m_moveSpeed = QVector3D();

    m_zoomFactor = 0.F;
}

void OpenGLCamera::update()
{
    m_angle += m_angleSpeed;

    if (m_angle.x() > 90.F) {
        m_angle.setX(90.F);
    } else if (m_angle.x() < -90.F) {
        m_angle.setX(-90.F);
    }

    m_track += getQuaternion().rotatedVector(m_moveSpeed / getZoomLevel());

    m_angleSpeed = QVector2D();
    m_moveSpeed = QVector2D();
}

void OpenGLCamera::setMoveSpeed(const QVector2D& moveSpeed)
{
    m_moveSpeed = moveSpeed;
}

void OpenGLCamera::setAngleSpeed(const QVector2D& angleSpeed)
{
    m_angleSpeed = angleSpeed;
}

void OpenGLCamera::zoomIn(float zoomFactor)
{
    m_zoomFactor += zoomFactor;
}

void OpenGLCamera::setAngle(const QVector2D& angle)
{
    m_angle = angle;
    m_angleSpeed = QVector2D();
}

QQuaternion OpenGLCamera::getQuaternion() const
{
    return QQuaternion::fromAxisAndAngle(1.F, 0.F, 0.F, 90.F) * QQuaternion::fromEulerAngles(m_angle);
}

QMatrix4x4 OpenGLCamera::getViewMatrix() const
{
    QMatrix4x4 viewMat;
    viewMat.translate(0.F, 0.F, -viewDistance * .5F);
    viewMat.scale(getZoomLevel());
    viewMat.rotate(getQuaternion().conjugated());
    viewMat.translate(-m_track);
    viewMat.scale(GeometryEngine::unitLength);

    return viewMat;
}

float OpenGLCamera::getZoomLevel() const
{
    return std::pow(2.F, m_zoomFactor);
}
