#ifndef OPENGLWINDOW_HPP
#define OPENGLWINDOW_HPP

#include "btlx.hpp"
#include "geometryengine.hpp"

#include <QBasicTimer>
#include <QMatrix4x4>
#include <QOpenGLDebugLogger>
#include <QOpenGLDebugMessage>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLWidget>
#include <QQuaternion>
#include <QVector2D>
#include <QVector3D>
#include <QWidget>

#include <memory>
#include <vector>

class OpenGLCamera {
public:
    void reset(const QVector3D& origin);
    void update();

    void setMoveSpeed(const QVector2D& moveSpeed);
    void setAngleSpeed(const QVector2D& angleSpeed);
    void zoomIn(float zoomFactor);

    void setAngle(const QVector2D& angle);

    QQuaternion getQuaternion() const;
    QMatrix4x4 getViewMatrix() const;

    float getZoomLevel() const;

private:
    QVector2D m_angle;
    QVector2D m_angleSpeed;

    QVector3D m_track;
    QVector3D m_moveSpeed;

    float m_zoomFactor{0.F};
};

class OpenGLWidget: public QOpenGLWidget {
    Q_OBJECT
public:
    explicit OpenGLWidget(const Btlx::Project& project, QWidget* parent = nullptr);
    virtual ~OpenGLWidget() noexcept;

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void updateGeometry();
    void updateGlobalTransform(std::size_t index, std::size_t transformIndex);
    void updateRawPartTransform(std::size_t index, std::size_t transformIndex);

public slots:
    void setToProjectView();
    void setToRawPartView(std::size_t index);
    void setToLinkView(std::size_t rawPartIndex, std::size_t partIndex);
    void setToPartView(std::size_t index);

    void setToFrontView();
    void setToRightView();
    void setToUpView();

protected:
    void timerEvent(QTimerEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;

private:
    QVector3D meanPoint() const;
    void draw(const QMatrix4x4& view, const QMatrix4x4& projection);

private:
    const Btlx::Project* m_project;

    std::unique_ptr<GeometryEngine> m_geometries;

    float m_aspect;

    Btlx::ItemType m_drawMode;
    std::size_t m_partIndex;
    std::size_t m_rawPartIndex;

    QBasicTimer m_timer;
    QVector2D m_mouseLastPosition;

    OpenGLCamera m_camera;

    QOpenGLDebugLogger m_logger;
};

#endif // OPENGLWINDOW_HPP
