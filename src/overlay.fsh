#ifdef GL_ES
precision mediump int;
precision mediump float;
#endif

uniform sampler2D u_texture;

varying vec2 v_tex_coord;

void main(void)
{
    vec4 tex_color = texture2D(u_texture, v_tex_coord);
    if (tex_color.a < 0.1) {
        discard;
    }
    gl_FragColor = tex_color;
}
