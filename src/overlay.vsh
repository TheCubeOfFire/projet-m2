#ifdef GL_ES
precision mediump int;
precision mediump float;
#endif

attribute vec3 a_position;
attribute vec2 a_tex_coord;

varying vec2 v_tex_coord;

void main(void)
{
    gl_Position = vec4(a_position, 1.);
    v_tex_coord = a_tex_coord;
}
