#include "treeitempart.hpp"

TreeItemPart::TreeItemPart(QTreeWidget* parent):
    QTreeWidgetItem(parent, ItemType::UserType),
    m_index(0),
    m_type(Btlx::ItemType::Project)
{
}

TreeItemPart::TreeItemPart(QTreeWidgetItem* parent, std::size_t index, Btlx::ItemType type, std::size_t transformIndex):
    QTreeWidgetItem(parent, ItemType::UserType),
    m_index(index),
    m_type(type),
    m_transformIndex(transformIndex)
{
}

std::size_t TreeItemPart::getIndex() const
{
    return m_index;
}

Btlx::ItemType TreeItemPart::getType() const
{
    return m_type;
}

std::size_t TreeItemPart::getTransformIndex() const
{
    return m_transformIndex;
}
