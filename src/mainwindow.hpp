#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "btlx.hpp"
#include "openglwidget.hpp"
#include "treeitempart.hpp"

#include <QMainWindow>
#include <QShortcut>
#include <QTreeWidgetItem>
#include <QWidget>
#include <QtCore>
#include <QtGui>

#include <cstddef>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow: public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

signals:
    void projectClosed();
    void projectLoaded();

private slots:
    void onActionOpenTriggered();
    void onActionSaveTriggered();
    void onActionQuitTriggered();

    void onDesignationLineEditEditingFinished();
    void onCountValueChanged(int value);

    void onGroupEditEditingFinished();
    void onStoreyEditEditingFinished();
    void onMaterialEditEditingFinished();

    void onNumberEditEditingFinished();
    void onOrderNumberEditEditingFinished();
    void onAssemblyNumberEditEditingFinished();
    void onElementNumberEditEditingFinished();

    void onOxSpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::refPoint, &QVector3D::setX);
    }

    void onOySpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::refPoint, &QVector3D::setY);
    }

    void onOzSpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::refPoint, &QVector3D::setZ);
    }

    void onXxSpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::xvector, &QVector3D::setX);
    }

    void onXySpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::xvector, &QVector3D::setY);
    }

    void onXzSpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::xvector, &QVector3D::setZ);
    }

    void onYxSpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::yvector, &QVector3D::setX);
    }

    void onYySpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::yvector, &QVector3D::setY);
    }

    void onYzSpinBoxValueChanged(double value)
    {
        onSpinBoxValueChanged(value, &Btlx::Position::yvector, &QVector3D::setZ);
    }

    void onTransformIDComboBoxItemChanged(int index);

    void onRedSpinBoxValueChanged(int value);
    void onGreenSpinBoxValueChanged(int value);
    void onBlueSpinBoxValueChanged(int value);
    void onTransparencySpinBoxValueChanged(int value);

    void onRefSideComboBoxIndexChanged(int index);
    void onAlignComboBoxIndexChanged(int index);

    void onTreeWidgetItemActivated(QTreeWidgetItem* itm, int column);
    void onSearchButtonClicked();

    void onProjectSelected();
    void onPartSelected(std::size_t index);
    void onRawPartSelected(std::size_t index);
    void onLinkSelected(std::size_t rawPartIndex, std::size_t partIndex);

private:
    void onSpinBoxValueChanged(double value, QVector3D Btlx::Position::* v, void (QVector3D::* func)(float));
    void setupConnections();
    void reset();

    void updateNames();

    TreeItemPart* addRoot(const QString& name);
    TreeItemPart* addChild(const QString& name, std::size_t index, const QString& number, Btlx::ItemType type, TreeItemPart *parent, std::size_t transformIndex = 0);

private:
    Ui::MainWindow* ui;
    OpenGLWidget* m_glWidget;
    Btlx::Project m_project;

    bool m_projectLoaded{false};
    Btlx::ItemType m_currentType{Btlx::ItemType::Project};
    std::size_t m_currentPartIndex{0};
    std::size_t m_currentRawPartIndex{0};
};
#endif // MAINWINDOW_HPP
