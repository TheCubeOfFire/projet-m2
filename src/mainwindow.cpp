#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include "btlx.hpp"
#include "openglwidget.hpp"
#include "treeitempart.hpp"
#include "loadsavexml.hpp"

#include <QtXml>

#include <QAction>
#include <QDir>
#include <QDoubleSpinBox>
#include <QFile>
#include <QFileDialog>
#include <QLineEdit>
#include <QList>
#include <QMdiArea>
#include <QMessageBox>
#include <QTextStream>
#include <QTreeWidget>
#include <QWidget>
#include <QXmlQuery>

#include <type_traits>
#include <utility>

MainWindow::MainWindow(QWidget* parent):
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_glWidget(nullptr)
{
    ui->setupUi(this);

    auto* mdiArea = new QMdiArea(this);

    m_glWidget = new OpenGLWidget(m_project, parent);

    mdiArea->addSubWindow(m_glWidget);
    m_glWidget->showMaximized();
    this->setWindowTitle("BtlX");
    setCentralWidget(mdiArea);

    setupConnections();
}

void MainWindow::setupConnections()
{
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::onActionOpenTriggered);
    connect(ui->actionSave, &QAction::triggered, this, &MainWindow::onActionSaveTriggered);
    connect(ui->actionQuit, &QAction::triggered, this, &MainWindow::onActionQuitTriggered);

    connect(ui->action3DFront, &QAction::triggered, m_glWidget, &OpenGLWidget::setToFrontView);
    connect(ui->action3DRight, &QAction::triggered, m_glWidget, &OpenGLWidget::setToRightView);
    connect(ui->action3DUp, &QAction::triggered, m_glWidget, &OpenGLWidget::setToUpView);

    connect(this, &MainWindow::projectClosed, this, &MainWindow::reset);
    connect(this, &MainWindow::projectClosed, m_glWidget, &OpenGLWidget::updateGeometry);
    connect(this, &MainWindow::projectLoaded, m_glWidget, &OpenGLWidget::updateGeometry);

    connect(ui->designationLineEdit, &QLineEdit::editingFinished, this, &MainWindow::onDesignationLineEditEditingFinished);
    connect(ui->countSpinBox, qOverload<int>(&QSpinBox::valueChanged), this, &MainWindow::onCountValueChanged);

    connect(ui->groupEdit, &QLineEdit::editingFinished, this, &MainWindow::onGroupEditEditingFinished);
    connect(ui->storeyEdit, &QLineEdit::editingFinished, this, &MainWindow::onStoreyEditEditingFinished);
    connect(ui->materialEdit, &QLineEdit::editingFinished, this, &MainWindow::onMaterialEditEditingFinished);

    connect(ui->numberEdit, &QLineEdit::editingFinished, this, &MainWindow::onNumberEditEditingFinished);
    connect(ui->orderNumberEdit, &QLineEdit::editingFinished, this, &MainWindow::onOrderNumberEditEditingFinished);
    connect(ui->assemblyNumberEdit, &QLineEdit::editingFinished, this, &MainWindow::onAssemblyNumberEditEditingFinished);
    connect(ui->elementNumberEdit, &QLineEdit::editingFinished, this, &MainWindow::onElementNumberEditEditingFinished);

    connect(ui->linkOxSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onOxSpinBoxValueChanged);
    connect(ui->linkOySpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onOySpinBoxValueChanged);
    connect(ui->linkOzSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onOzSpinBoxValueChanged);

    connect(ui->linkXxSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onXxSpinBoxValueChanged);
    connect(ui->linkXySpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onXySpinBoxValueChanged);
    connect(ui->linkXzSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onXzSpinBoxValueChanged);

    connect(ui->linkYxSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onYxSpinBoxValueChanged);
    connect(ui->linkYySpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onYySpinBoxValueChanged);
    connect(ui->linkYzSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onYzSpinBoxValueChanged);

    connect(ui->partOxSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onOxSpinBoxValueChanged);
    connect(ui->partOySpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onOySpinBoxValueChanged);
    connect(ui->partOzSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onOzSpinBoxValueChanged);

    connect(ui->partXxSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onXxSpinBoxValueChanged);
    connect(ui->partXySpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onXySpinBoxValueChanged);
    connect(ui->partXzSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onXzSpinBoxValueChanged);

    connect(ui->partYxSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onYxSpinBoxValueChanged);
    connect(ui->partYySpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onYySpinBoxValueChanged);
    connect(ui->partYzSpinBox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &MainWindow::onYzSpinBoxValueChanged);

    connect(ui->transformIDComboBox, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::onTransformIDComboBoxItemChanged);

    connect(ui->redSpinBox, qOverload<int>(&QSpinBox::valueChanged), this, &MainWindow::onRedSpinBoxValueChanged);
    connect(ui->greenSpinBox, qOverload<int>(&QSpinBox::valueChanged), this, &MainWindow::onGreenSpinBoxValueChanged);
    connect(ui->blueSpinBox, qOverload<int>(&QSpinBox::valueChanged), this, &MainWindow::onBlueSpinBoxValueChanged);
    connect(ui->transparencySpinBox, qOverload<int>(&QSpinBox::valueChanged), this, &MainWindow::onTransparencySpinBoxValueChanged);

    connect(ui->refSideComboBox, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::onRefSideComboBoxIndexChanged);
    connect(ui->alignComboBox, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::onAlignComboBoxIndexChanged);

    connect(ui->treeWidget, &QTreeWidget::itemClicked, this, &MainWindow::onTreeWidgetItemActivated);
    connect(ui->searchButton, &QPushButton::clicked, this, &MainWindow::onSearchButtonClicked);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onProjectSelected()
{
    Q_ASSERT(m_projectLoaded);
    m_currentType = Btlx::ItemType::Project;
    m_glWidget->setToProjectView();

    ui->typeLabel->setText("Project");
    ui->editStackedWidget->setCurrentWidget(ui->projectPage);
}

void MainWindow::onPartSelected(std::size_t index)
{
    m_currentType = Btlx::ItemType::Part;
    m_currentPartIndex = index;
    m_glWidget->setToPartView(index);

    const Btlx::Part& part = m_project.parts[index];

    ui->designationLineEdit->setText(part.designation);
    ui->countSpinBox->setValue(part.count);

    ui->groupEdit->setText(part.group);
    ui->storeyEdit->setText(part.storey);
    ui->materialEdit->setText(part.material);

    ui->numberEdit->setText(QString::number(part.number));
    ui->orderNumberEdit->setText(QString::number(part.ordernumber));
    ui->assemblyNumberEdit->setText(QString::number(part.assemblynumber));
    ui->elementNumberEdit->setText(QString::number(part.elementnumber));

    ui->widthSpinBox->setValue(part.size.x());
    ui->heightSpinBox->setValue(part.size.y());
    ui->depthSpinBox->setValue(part.size.z());

    ui->colorGroupBox->setEnabled(true);
    ui->redSpinBox->setValue(part.color.red);
    ui->greenSpinBox->setValue(part.color.green);
    ui->blueSpinBox->setValue(part.color.blue);
    ui->transparencySpinBox->setValue(part.color.transparency);

    ui->refSideComboBox->setCurrentIndex(part.rf.side - 1);
    if (part.rf.align == "yes") {
        ui->alignComboBox->setCurrentIndex(0);
    } else {
        ui->alignComboBox->setCurrentIndex(1);
    }

    ui->transformIDComboBox->clear();
    if (part.transformation.isEmpty()) {
        ui->transformationGroupBox->setEnabled(false);
    } else {
        ui->transformationGroupBox->setEnabled(true);
        for (int i = 0; i < part.transformation.size(); ++i) {
            ui->transformIDComboBox->addItem(QString::number(i + 1), i);
        }
        ui->transformIDComboBox->setCurrentIndex(0);
    }

    ui->typeLabel->setText("Part");
    ui->editStackedWidget->setCurrentWidget(ui->partPage);
}

void MainWindow::onRawPartSelected(std::size_t index)
{
    m_currentType = Btlx::ItemType::RawPart;
    m_currentRawPartIndex = index;
    m_glWidget->setToRawPartView(index);

    const Btlx::RawPart& rpart = m_project.rparts[index];

    ui->designationLineEdit->setText(rpart.designation);
    ui->countSpinBox->setValue(rpart.count);

    ui->groupEdit->setText("");
    ui->storeyEdit->setText("");
    ui->materialEdit->setText(rpart.material);

    ui->numberEdit->setText(QString::number(rpart.number));
    ui->orderNumberEdit->setText("");
    ui->assemblyNumberEdit->setText("");
    ui->elementNumberEdit->setText(QString::number(rpart.elementnumber));

    ui->widthSpinBox->setValue(rpart.size.x());
    ui->heightSpinBox->setValue(rpart.size.y());
    ui->depthSpinBox->setValue(rpart.size.z());

    ui->colorGroupBox->setEnabled(false);

    ui->refSideComboBox->setCurrentIndex(rpart.rf.side - 1);
    if (rpart.rf.align == "yes") {
        ui->alignComboBox->setCurrentIndex(0);
    } else {
        ui->alignComboBox->setCurrentIndex(1);
    }

    ui->transformIDComboBox->clear();
    if (rpart.transformation.isEmpty()) {
        ui->transformationGroupBox->setEnabled(false);
    } else {
        ui->transformationGroupBox->setEnabled(true);
        for (int i = 0; i < rpart.transformation.size(); ++i) {
            ui->transformIDComboBox->addItem(QString::number(i + 1), i);
        }
        ui->transformIDComboBox->setCurrentIndex(0);
    }

    ui->typeLabel->setText("Raw Part");
    ui->editStackedWidget->setCurrentWidget(ui->partPage);
}

void MainWindow::onLinkSelected(std::size_t rawPartIndex, std::size_t partIndex)
{
    Q_ASSERT(m_projectLoaded);
    m_currentType = Btlx::ItemType::PartLink;
    m_currentRawPartIndex = rawPartIndex;
    m_currentPartIndex = partIndex;
    m_glWidget->setToLinkView(rawPartIndex, partIndex);

    Q_ASSERT(m_project.rparts.size() > rawPartIndex);
    Q_ASSERT(m_project.rparts[rawPartIndex].partref.size() > static_cast<int>(partIndex));
    const Btlx::Position& p = m_project.rparts[rawPartIndex].partref[static_cast<int>(partIndex)];

    ui->linkOxSpinBox->setValue(p.refPoint.x());
    ui->linkOySpinBox->setValue(p.refPoint.y());
    ui->linkOzSpinBox->setValue(p.refPoint.z());

    ui->linkXxSpinBox->setValue(p.xvector.x());
    ui->linkXySpinBox->setValue(p.xvector.y());
    ui->linkXzSpinBox->setValue(p.xvector.z());

    ui->linkYxSpinBox->setValue(p.yvector.x());
    ui->linkYySpinBox->setValue(p.yvector.y());
    ui->linkYzSpinBox->setValue(p.yvector.z());

    ui->typeLabel->setText("Link");
    ui->editStackedWidget->setCurrentWidget(ui->linkPage);
}

void MainWindow::onActionOpenTriggered() try
{
    QString filter = "BTLX File (*.btlx)";
    QDomDocument partDocument;
    QList<Btlx::Part> parties;
    QList<Btlx::RawPart>rparties;
    QUrl file_url = QFileDialog::getOpenFileUrl(this, "Ouvrir un fichier", QUrl::fromLocalFile(QDir::homePath()), filter);
    if (file_url == QUrl{}) {
        return;
    }

    emit projectClosed();

    m_project.emplacement = file_url;

    QXmlQuery queryPart;
    m_project = load::loadfile(file_url);


    TreeItemPart *project = addRoot("Project");
    for (std::size_t i = 0; i < m_project.rparts.size(); ++i) {
        auto& rawP = m_project.rparts[i];
        TreeItemPart *rawpart = addChild(rawP.designation, i, QString::number(rawP.number), Btlx::ItemType::RawPart, project);
        for (std::size_t j = 0; j < rawP.links.size(); ++j) {
            auto& link = rawP.links[j];
            auto& part = m_project.parts[link.partIndex];
            TreeItemPart *linkPart = addChild(part.designation, i, QString::number(part.number), Btlx::ItemType::PartLink, rawpart, j);
            addChild(part.designation, link.partIndex, QString::number(part.number), Btlx::ItemType::Part, linkPart);
        }
    }

    m_projectLoaded = true;
    emit projectLoaded();
} catch (const std::exception& e) {
    QMessageBox::critical(this, "Le fichier n'a pas pu être importé", e.what());
}

void MainWindow::onActionSaveTriggered()
{

    QString filter = "BTLX File (*.btlx)";
    QString fileName = QFileDialog::getSaveFileName(this, "Enregistrer sous", QDir::homePath(), filter);
    if (fileName.isEmpty()) {
        return;
    }

    QFile xml_doc(fileName);
    if (!xml_doc.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QMessageBox::warning(this, "Erreur sauvegarde", "Le fichier ne parvient pas à être enregistré");
        return;
    }

    save::saveProject(m_project, xml_doc);
}

void MainWindow::onTreeWidgetItemActivated(QTreeWidgetItem* itm, int /*column*/)
{
    auto* partItem = static_cast<TreeItemPart*>(itm);
    std::size_t index = partItem->getIndex();

    switch (partItem->getType()) {
    case Btlx::ItemType::Project:
        onProjectSelected();
        break;

    case Btlx::ItemType::RawPart:
        onRawPartSelected(index);
        break;

    case Btlx::ItemType::PartLink:
        onLinkSelected(index, partItem->getTransformIndex());
        break;

    case Btlx::ItemType::Part:
        onPartSelected(index);
        break;
    }
}

void MainWindow::reset()
{
    m_projectLoaded = false;
    m_project = Btlx::Project{};
    ui->treeWidget->clear();
    ui->editStackedWidget->setCurrentWidget(ui->projectPage);
    emit projectLoaded();
}

void MainWindow::updateNames()
{
    for (QTreeWidgetItemIterator it(ui->treeWidget); *it; ++it) {
        TreeItemPart* itm = static_cast<TreeItemPart*>(*it);
        switch (itm->getType()) {
        case Btlx::ItemType::Project:
            break;

        case Btlx::ItemType::Part: {
            const auto& p = m_project.parts[itm->getIndex()];
            itm->setText(0, QString::number(p.number) + ": " + p.designation);
            break;
        }

        case Btlx::ItemType::RawPart: {
            const auto& p = m_project.rparts[itm->getIndex()];
            itm->setText(0, QString::number(p.number) + ": " + p.designation);
            break;
        }

        case Btlx::ItemType::PartLink: {
            const auto& rpart = m_project.rparts[itm->getIndex()];
            const auto& link = rpart.links[itm->getTransformIndex()];
            const auto& p = m_project.parts[link.partIndex];
            itm->setText(0, QString::number(p.number) + ": " + p.designation);
            break;
        }
        }
    }
}

void MainWindow::onActionQuitTriggered()
{
    QApplication::quit();
}

void MainWindow::onDesignationLineEditEditingFinished()
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].designation = ui->designationLineEdit->text();
    } else if (m_currentType == Btlx::ItemType::RawPart) {
        m_project.rparts[m_currentRawPartIndex].designation = ui->designationLineEdit->text();
    }
    updateNames();
}

void MainWindow::onCountValueChanged(int value)
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].count = value;
    } else if (m_currentType == Btlx::ItemType::RawPart) {
        m_project.rparts[m_currentRawPartIndex].count = value;
    }
}

void MainWindow::onGroupEditEditingFinished()
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].group = ui->groupEdit->text();
    }
}

void MainWindow::onStoreyEditEditingFinished()
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].storey = ui->storeyEdit->text();
    }
}

void MainWindow::onMaterialEditEditingFinished()
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].material = ui->materialEdit->text();
    } else if (m_currentType == Btlx::ItemType::RawPart) {
        m_project.rparts[m_currentRawPartIndex].material = ui->materialEdit->text();
    }
}

void MainWindow::onNumberEditEditingFinished()
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].number = ui->numberEdit->text().toInt();
    } else if (m_currentType == Btlx::ItemType::RawPart) {
        m_project.rparts[m_currentRawPartIndex].number = ui->numberEdit->text().toInt();
    }
    updateNames();
}

void MainWindow::onOrderNumberEditEditingFinished()
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].ordernumber = ui->orderNumberEdit->text().toInt();
    }
}

void MainWindow::onAssemblyNumberEditEditingFinished()
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].assemblynumber = ui->assemblyNumberEdit->text().toInt();
    }
}

void MainWindow::onElementNumberEditEditingFinished()
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].elementnumber = ui->elementNumberEdit->text().toInt();
    } else if (m_currentType == Btlx::ItemType::RawPart) {
        m_project.rparts[m_currentRawPartIndex].elementnumber = ui->elementNumberEdit->text().toInt();
    }
}

void MainWindow::onTransformIDComboBoxItemChanged(int index)
{
    int data = ui->transformIDComboBox->itemData(index).toInt();
    const Btlx::Position& p = (m_currentType == Btlx::ItemType::Part) ?
                m_project.parts[m_currentPartIndex].transformation[data] :
                m_project.rparts[m_currentRawPartIndex].transformation[data];

    ui->partOxSpinBox->setValue(p.refPoint.x());
    ui->partOySpinBox->setValue(p.refPoint.y());
    ui->partOzSpinBox->setValue(p.refPoint.z());

    ui->partXxSpinBox->setValue(p.xvector.x());
    ui->partXySpinBox->setValue(p.xvector.y());
    ui->partXzSpinBox->setValue(p.xvector.z());

    ui->partYxSpinBox->setValue(p.yvector.x());
    ui->partYySpinBox->setValue(p.yvector.y());
    ui->partYzSpinBox->setValue(p.yvector.z());
}

void MainWindow::onRedSpinBoxValueChanged(int value)
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].color.red = value;
    }
}

void MainWindow::onGreenSpinBoxValueChanged(int value)
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].color.green = value;
    }
}

void MainWindow::onBlueSpinBoxValueChanged(int value)
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].color.blue = value;
    }
}

void MainWindow::onTransparencySpinBoxValueChanged(int value)
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].color.transparency = value;
    }
}

void MainWindow::onRefSideComboBoxIndexChanged(int /*index*/)
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].rf.side = ui->refSideComboBox->currentText().toInt();
    } else if (m_currentType == Btlx::ItemType::RawPart) {
        m_project.rparts[m_currentRawPartIndex].rf.side = ui->refSideComboBox->currentText().toInt();
    }
}

void MainWindow::onAlignComboBoxIndexChanged(int index)
{
    if (!m_projectLoaded) {
        return;
    }

    if (m_currentType == Btlx::ItemType::Part) {
        m_project.parts[m_currentPartIndex].rf.align = (index == 0) ? "yes" : "no";
    } else if (m_currentType == Btlx::ItemType::RawPart) {
        m_project.rparts[m_currentRawPartIndex].rf.align = (index == 0) ? "yes" : "no";
    }
}

void MainWindow::onSpinBoxValueChanged(double value, QVector3D Btlx::Position::* v, void (QVector3D::* func)(float))
{
    if (!m_projectLoaded) {
        return;
    }

    int currentTransformIndex = ui->transformIDComboBox->currentData().toInt();
    switch (m_currentType) {
    case Btlx::ItemType::PartLink: {
        auto& partref = m_project.rparts[m_currentRawPartIndex].partref;
        Q_ASSERT(partref.size() > static_cast<int>(m_currentPartIndex));
        // apply function func on QVector3D on Position v on current partref
        ((partref[static_cast<int>(m_currentPartIndex)].*v).*func)(static_cast<float>(value));
        m_glWidget->updateRawPartTransform(m_currentRawPartIndex, m_currentPartIndex);
        break;
    }

    case Btlx::ItemType::Part: {
        auto& transformation = m_project.parts[m_currentPartIndex].transformation;
        Q_ASSERT(transformation.size() > static_cast<int>(currentTransformIndex));
        ((transformation[currentTransformIndex].*v).*func)(static_cast<float>(value));
        m_glWidget->updateGlobalTransform(m_currentPartIndex, currentTransformIndex);
        break;
    }

    case Btlx::ItemType::RawPart: {
        auto& transformation = m_project.rparts[m_currentRawPartIndex].transformation;
        Q_ASSERT(transformation.size() > static_cast<int>(currentTransformIndex));
        ((transformation[currentTransformIndex].*v).*func)(static_cast<float>(value));
        break;
    }

    default:
       break;
    }
}

TreeItemPart* MainWindow::addRoot(const QString& name)
{
    auto* itm = new TreeItemPart(ui->treeWidget);
    itm->setText(0, name);
    ui->treeWidget->addTopLevelItem(itm);
    return itm;
}

TreeItemPart* MainWindow::addChild(const QString& name, std::size_t index, const QString& number, Btlx::ItemType type, TreeItemPart *parent, std::size_t transformIndex)
{
    auto* itm = new TreeItemPart(parent, index, type, transformIndex);
    QString namePiece = number + ": " + name;
    itm->setText(0, namePiece);
    parent->addChild(itm);
    return itm;
}

void MainWindow::onSearchButtonClicked()
{
    qDebug() << ui->searchLineEdit->text();
}

