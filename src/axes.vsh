#ifdef GL_ES
precision mediump int;
precision mediump float;
#endif

uniform mat4 u_projection;
uniform mat4 u_view;

attribute vec3 a_position;
attribute vec3 a_color;

varying vec3 v_color;

void main(void)
{
    v_color = a_color;
    gl_Position = u_projection * u_view * vec4(a_position, 1.);
}
